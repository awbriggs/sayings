FROM openfaas/of-watchdog:0.5.5 AS watchdog
FROM alpine:3.10 AS runtime


COPY build/ /home/app/public/

COPY --from=watchdog /fwatchdog /usr/bin/fwatchdog
RUN chmod +x /usr/bin/fwatchdog

WORKDIR /home/app
COPY ./function.yml .

WORKDIR /home/app

ENV mode="static"
ENV static_path="/home/app/public"

HEALTHCHECK --interval=3s CMD [ -e /tmp/.lock ] || exit 1

CMD ["/usr/bin/fwatchdog"]
