export enum RelevantKeyCodes {
    ESCAPE = 27,
    UP_KEY = 38,
    DOWN_KEY = 40
}
