import React, {Component} from "react";

import './styles.css';
import OrganizationCard from "./OrganizationCard";

type Props = {
    displaySectionCallback(key: number): void,
    handleNameInputChange(newName: string): void,
    currentlySelectedNumber: number,
    names: Array<string>,
    destroy(index: number): void,
    addCard(): void,
}

class SideBar extends Component<Props> {
    render() {
        return (
            <div className="sidebar">
                <div className="inner-sidebar-grid">
                    <div className="card-organizer-card-grid">
                        {this.props.names.map((name, index) => {
                            return <OrganizationCard destroyCardCallback={this.props.destroy}
                                                     name={name} clickKey={index} key={index}
                                                     currentlySelected={(index === this.props.currentlySelectedNumber)}
                                                     handleNameInputChange={this.props.handleNameInputChange}
                                                     clickOfOrganizationCard={this.props.displaySectionCallback}
                                                     index={index}/>
                        })}
                        <button className="adding-card-button" onClick={event => {
                            event.preventDefault();
                            this.props.addCard()
                        }}>+
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default SideBar;
