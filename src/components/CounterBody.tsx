import React, {Component} from 'react';

import './styles.css';
import CountingCard from "./CountingCard";
import AddingCard from "./AddingCard";
import {CardHolder} from "../App";

type Props = {
    holder: CardHolder,
    destroyCard(key: number): void,
    addCard(name: string): void,
    updateCard(key: number, value: number): void
}

class CounterBody extends Component<Props> {

    render() {
        return (
            <div className="body">
                <div className="card-grid">
                    {this.props.holder.cards && this.props.holder.cards.map((card, index) => {
                        return <CountingCard handleClick={this.props.updateCard}
                                             count={card.currentCount} counting={card.name}
                                             cardKey={index} key={index}
                                             destroyCardCallback={this.props.destroyCard}/>
                    })}
                    <AddingCard addCardCallback={this.props.addCard}/>
                </div>
            </div>
        );
    }
}

export default CounterBody;
