import React from "react";
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CounterBody from "../../components/CounterBody";

Enzyme.configure({adapter: new Adapter() });

it('Cards being made', () => {
    const component = mount(<CounterBody addCard={jest.fn()} destroyCard={jest.fn()}
                                         holder={{name: "testing", cards: []}} updateCard={jest.fn()}/>);
    expect(component.find(".card").length).toBe(1);
    component.setProps({holder: {name: "asdf", cards: [{name: "ROUND ONE", count: "5"}]}});
    expect(component.find(".card").length).toBe(2);
});


it('Cards being deleted', () => {
    const component = mount(<CounterBody addCard={jest.fn()} destroyCard={jest.fn()}
                                         holder={{name: "test", cards: [{name: "testing", currentCount: 5}]}}
                                         updateCard={jest.fn()}/>);

    expect(component.find(".card").length).toBe(2);
    component.setProps({holder: {name: "whatever", cards: []}});
    expect(component.find(".card").length).toBe(1);
});

it('exit called on exit button click', () => {
    const mockDestroy = jest.fn();
    const component = mount(<CounterBody addCard={jest.fn()} destroyCard={mockDestroy}
                                         holder={{name: "test", cards: [{name: "testing", currentCount: 5}]}}
                                         updateCard={jest.fn()}/>);

    component.find('.card').at(0).find('.card-escape-button').simulate('click');
    expect(mockDestroy).toBeDefined()
});
