import React from "react";
import AddingCard from "../../components/AddingCard";
import Enzyme, {shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({adapter: new Adapter() });

it('Calls back with correct name', () => {
    const mockFn = jest.fn();
    const component = shallow(<AddingCard addCardCallback={mockFn} />);
    component.find('.card-add-input').simulate('change',
        {target: {value: 'Hey whats up'}})

    component.find(".card-text-button").simulate('click')

    expect(mockFn).toHaveBeenCalledWith("Hey whats up");
    expect(component).toMatchSnapshot()
});


it('No callback with empty value', () => {
    const mockFn = jest.fn();
    const component = shallow(<AddingCard addCardCallback={mockFn} />);

    component.find(".card-text-button").simulate('click')

    expect(mockFn).toHaveBeenCalledTimes(0);
});

it('Expect error class to be added on failure', () => {
    const mockFn = jest.fn();
    const component = shallow(<AddingCard addCardCallback={mockFn}/>)

    expect(component.find('.error-text-input')).toBeDefined()
})
